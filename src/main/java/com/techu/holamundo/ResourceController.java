package com.techu.holamundo;

import org.springframework.web.bind.annotation.*;

@RestController
public class ResourceController {

    @GetMapping("/saludos")
    public String saludar(@RequestParam(value = "nombre", defaultValue = "Tech U!") String name){
        return String.format("Hola %s ;)", name);
    }
}
